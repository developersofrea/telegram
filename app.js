process.env.NODE_PATH = '.';
require('module').Module._initPaths();
/**
 * Starting our application
 */
require('app/core/bootstrap');

var kernel = app.make('Kernel').handle();
