var ConfigManager = require('./ConfigManager.js'),
	ServiceProvider = require('services/foundation/ServiceProvider');

class ConfigServiceProvider extends ServiceProvider {
	boot() {
		//
	}

	register() {
		this.app.singleton('Config', function (app) {
			return new ConfigManager();
		});
	}
}

module.exports = ConfigServiceProvider;