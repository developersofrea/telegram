class ConfigManager {

	get(key) {
		let config = false;
		try {
			config = require('app/config/' + key);
		}
		catch(e) {}
		return config;
	}

}

module.exports = ConfigManager;