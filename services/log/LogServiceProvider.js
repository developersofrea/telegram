var ServiceProvider = require('services/foundation/ServiceProvider'),
	Log = require('./Log');

class LogServiceProvider extends ServiceProvider {
	boot() {
		//
	}

	register() {
		this.app.singleton('Log', function () {
			return new Log();
		});
	}
}

module.exports = LogServiceProvider;