var Telegram = require('./Telegram'),
	ServiceProvider = require('services/foundation/ServiceProvider');

class TelegramServiceProvider extends ServiceProvider {
	boot() {
		//
	}

	register() {
		this.app.singleton('Telegram', function (app) {
			return new Telegram();
		});
	}
}

module.exports = TelegramServiceProvider;