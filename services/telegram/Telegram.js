var Telegraf = require('telegraf');

class Telegram {
	constructor() {
		Log.write('Booting bot');
		this.app = new Telegraf(Config.get('telegram').token);
		this.app.startPolling();
	}

	command(channel, action) {
		return this.app.command(channel, action);
	}

	hears(channel, action) {
		return this.app.hears(channel, action);
	}

	on(channel, action) {
		return this.app.on(channel, action);
	}	
}

module.exports = Telegram;
