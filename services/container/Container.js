class Container {
	properties() {
		this._bindings = {};
		this._instances = {};
		this._resolved = {};
		this._aliases = {};
	}

	instance(abstract, instance) {
		this._instances[abstract] = instance;
	}

	setAlias(abstract) {
		global[abstract] = this.make(abstract);
	}

	make(abstract) {
		if(typeof this._instances[abstract] !== 'undefined') {
			return this._instances[abstract];
		}
		var concrete = this.getConcrete(abstract)(this);
		if(this.isShared(abstract)) {
			this._instances[abstract] = concrete;
		}
		this._resolved[abstract] = true;
		return concrete;
	}

	isShared(abstract) {
		return typeof this._bindings[abstract]['shared'] !== 'undefined' && this._bindings[abstract]['shared'] === true;
	}

	getConcrete(abstract) {
		if(typeof this._bindings[abstract] !== 'undefined') {
			return this._bindings[abstract]['concrete'];
		}
		return abstract;
	}

	bind(abstract, concrete, shared = false) {
		delete this._instances[abstract];
		if(typeof concrete == 'undefined') {
			concrete = abstract;
		}
		this._bindings[abstract] = {
			concrete,
			shared
		};
	}

	singleton(abstract, concrete) {
		return this.bind(abstract, concrete, true);
	}

	resolved(abstract) {
		return typeof this.resolved[abstract] !== 'undefined' ||
			typeof this.instances[abstract] !== 'undefined';
	}

	bound(abstract) {
		return typeof this._bindings[abstract] !== 'undefined' ||
			typeof this._instances[abstract] !== 'undefined';
	}
}

module.exports = Container;