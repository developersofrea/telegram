var Container = require('services/container/Container');

class Application extends Container {
	constructor() {
		super();
		this.properties();
		this.registerBaseBindings();
		this.registerBaseServiceProviders();
		this.registerCoreAliases();
	}

	properties() {
		super.properties();
		this.booted = false;
		this.hasBeenBootstrapped = false;
		this.serviceProviders = [];
	}

	registerBaseBindings() {
		this.instance('app', this);
	}

	registerBaseServiceProviders() {
		this.register('services/log/LogServiceProvider');
		this.register('services/config/ConfigServiceProvider');
	}

	registerCoreAliases() {
		var aliases = {
			'App': 'app',
			'Log': 'Log',
			'Config': 'Config'
		};
		for(var i in aliases) {
			this.setAlias(aliases[i]);
		}
	}

	register(provider) {
		var registered = this.getProvider(provider)
		if(registered) {
			return registered;
		}
		var executed = new (require(provider))(this);
		this.registerProvider(executed);
		this.markAsRegistered(executed);
		return executed;
	}

	registerProvider(provider) {
		if(typeof provider.register === 'function') {
			provider.register();
		}
	}

	markAsRegistered(provider) {
		this.serviceProviders.push(provider);
	}

	bootstrapWith(bootstrappers) {
		this.hasBeenBootstrapped = true;
		for(var i in bootstrappers) {
			(new (require(bootstrappers[i]))).bootstrap(this);
		}
	}

	getProvider(provider) {
		return this.serviceProviders.find(function (element) {
			return element.name == provider;
		});
	}

	boot() {
		if(this.booted) {
			return false;
		}
		for(var i in this.serviceProviders) {
			this.bootProvider(this.serviceProviders[i]);
		}
		this.booted = true;
	}

	bootProvider(provider) {
		if(typeof provider.register === 'function') {
			provider.boot();
		}
	}
}

module.exports = Application;