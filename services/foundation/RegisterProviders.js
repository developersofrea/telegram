class RegisterProviders {
	bootstrap(app) {
		for(var i in Config.get('app').providers) {
			app.register(Config.get('app').providers[i]);
		}
		Log.write('Providers registered');
	}
}

module.exports = RegisterProviders;