class Kernel {
	constructor(app) {
		this.app = app;
	}

	handle() {
		this.bootstrap();
	}

	bootstrap() {
		if(! this.app.hasBeenBootstrapped) {
			this.app.bootstrapWith(this.getBootstrappers());
		}
	}

	getBootstrappers() {
		return [
			'services/foundation/RegisterProviders',
			'services/foundation/ShareAliases',
			'services/foundation/BootProviders',
		];
	}
}
module.exports = Kernel;