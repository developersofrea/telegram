class BootProviders {
	bootstrap(app) {
		app.boot();
		Log.write('Providers booted');
	}
}

module.exports = BootProviders;