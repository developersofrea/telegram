class ShareAliases {
	bootstrap(app) {
		for(var i in Config.get('app').aliases) {
			app.setAlias(Config.get('app').aliases[i]);
		}
		Log.write('Aliases have been shared.');
	}
}

module.exports = ShareAliases;