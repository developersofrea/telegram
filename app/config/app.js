module.exports = {
	version: process.env.VERSION,
	providers: [
		'services/telegram/TelegramServiceProvider',
		'app/providers/AppServiceProvider',
	],
	aliases: {
		'Telegram': 'Telegram'
	}
}