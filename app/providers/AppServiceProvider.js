var ServiceProvider = require('services/foundation/ServiceProvider');

class AppServiceProvider extends ServiceProvider {
	boot() {
		require('app/Commands');
	}

	register() {
		//
	}
}

module.exports = AppServiceProvider;