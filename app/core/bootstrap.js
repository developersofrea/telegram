var Application = require('services/foundation/Application'),
	Kernel = require('./kernel');

var app = new Application();
app.singleton('Kernel', function (app) {
	return new Kernel(app);
});
